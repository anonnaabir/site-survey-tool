import 'package:flutter/material.dart';

// void main() => runApp(const ProjectData());

class ProjectData extends StatelessWidget {
  // const ProjectData({super.key});
  final String projectid;
  const ProjectData({super.key, required this.projectid});

//   final String projectid;
//
//   const ProjectData({
//     Key? key,
//     required this.projectid,
// }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            title: Text('Projects Data'),
          ),
          body: Center(
            child: InkWell(
              child: Text("Edit Project"),
              onTap: () {
                Navigator.pushNamed(context, '/new-project');
              },
            ),
          ),
    );
  }
}



// class ProjectData extends StatefulWidget {
//   const Projects({ Key? key }) : super(key: key);
//
//   @override
//   State<Projects> createState() => _Projects();
// }
//
// class _Projects extends State<Projects> {
//
//   final List<String> projects = <String>[
//     'Project 01',
//     'Project 02',
//     'Project 03'
//   ];
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           title: Text('Projects'),
//         ),
//         body: Center(
//           child: ListView.builder(
//               itemCount: projects.length,
//               itemBuilder: (BuildContext context,int index){
//                 return ListTile(
//                   title: Text(projects[index]),
//                   onTap: () {
//                     const snackBar = SnackBar(content: Text('Tap'));
//
//                     ScaffoldMessenger.of(context).showSnackBar(snackBar);
//                   },
//                 );
//               }),
//         ));
//   }
// }