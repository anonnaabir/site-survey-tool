import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:sitesurveytool/add-data.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:sitesurveytool/project-data.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:path/path.dart' as p;

import 'model/get-projectdata.dart';

void main() => runApp(const Projects());


class Projects extends StatefulWidget {
  const Projects({ Key? key }) : super(key: key);

  @override
  State<Projects> createState() => _Projects();
}

class _Projects extends State<Projects> {

  List<String> projectNameList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text("Projects"),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.add_sharp),
            padding: const EdgeInsets.only(right: 40.0),
            onPressed: () => makePDF(),
          ),
        ],
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('users').doc(FirebaseAuth.instance.currentUser?.uid).collection('projects').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          return ListView(
            children: snapshot.data!.docs.map((document) {

                 return ListTile(
                   // leading: CircleAvatar(backgroundImage: NetworkImage(('https://picsum.photos/250?image=9'),),
                   title: Text(document['name']),
                    onTap: () {

                      String projectid = document['id'];
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => ProjectData(projectid: projectid),
                      ));
                    },
                    onLongPress: () {
                      final snackBar = SnackBar(
                        content: const Text('Do you want to delete this project?'),
                        action: SnackBarAction(
                          label: 'Yes',
                          // onPressed: () => DeleteProject(projectid),
                          onPressed: () {

                            Future DeleteProject() async {
                              // final siteUser = FirebaseFirestore.instance
                              //     .collection('projects')
                              //     .doc(document['id']);

                              final siteUser = FirebaseFirestore.instance
                                  .collection('users')
                                  .doc(FirebaseAuth.instance.currentUser?.uid)
                                  .collection('projects')
                                  .doc(document['id']);

                              // final instance = FirebaseFirestore.instance;
                              // final batch = instance.batch();
                              // var collection = instance
                              //     .collection('projects')
                              //     .doc(document['id'])
                              //     .collection('project-data');
                              // var snapshots = await collection.get();
                              // for (var doc in snapshots.docs) {
                              //   batch.delete(doc.reference);
                              // }

                              final instance = FirebaseFirestore.instance;
                              final batch = instance.batch();
                              var collection = instance
                                  .collection('users')
                                  .doc(FirebaseAuth.instance.currentUser?.uid)
                                  .collection('projects')
                                  .doc(document['id'])
                                  .collection('project-data');
                              var snapshots = await collection.get();
                              for (var doc in snapshots.docs) {
                                batch.delete(doc.reference);
                              }

                              await batch.commit();

                              siteUser.delete();

                              final deleteSucess = SnackBar(
                                content: const Text('Project Deleted'),
                              );

                              ScaffoldMessenger.of(context).showSnackBar(deleteSucess);

                            }

                            DeleteProject();

                          },
                        ),
                      );

                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      // Navigator.pushNamed(context, '/project-data');
                    },
                 );
            }).toList(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/new-project');
        },
        backgroundColor: Colors.green,
        child: const Icon(Icons.add_sharp),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: Color(0xFF6200EE),
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.white.withOpacity(.60),
          selectedFontSize: 14,
          unselectedFontSize: 14,
          onTap: (value) {
            Navigator.pushNamed(context, '/settings');
          },
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.favorite),
                label: 'School'
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.music_note),
                label: 'School'
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.location_on),
                label: 'School'
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.library_books),
                label: 'School'
            ),
          ],
        )
    );

  }

  Future<void> makePDF() async {

    QuerySnapshot querySnapshot = await FirebaseFirestore.instance
        .collection('users')
        .doc(FirebaseAuth.instance.currentUser?.uid)
        .collection('projects')
        .doc('Test_Project_01')
        .collection('project-data').get();

    // querySnapshot.docs.map(
    //         (doc) => UserTask(
    //         doc.data['id'],
    //         doc.data['Description'],
    //         etc...)
    // ).toList();

    for (int i = 0; i < querySnapshot.docs.length; i++) {
      final nameList = querySnapshot.docs[i];
      setState(() => this.projectNameList = nameList as List<String>);
      // setState(() => this.projectNameList = nameList);
      // setState(() => this.projectNameList = nameList as List<String>);
    }

    print(projectNameList);


    // final db = FirebaseFirestore.instance;
    //
    // db.collection('users')
    // .doc(FirebaseAuth.instance.currentUser?.uid)
    // .collection('projects')
    // .doc('Test_Project_01')
    // .collection('project-data').get().then(
    //       (res) => print("Successfully completed"),
    //   onError: (e) => print("Error completing: $e"),
    // );

    // final pdf = pw.Document();
    //
    // pdf.addPage(
    //   pw.Page(
    //     build: (pw.Context context) => pw.Column(
    //
    //     )
    //   ),
    // );
    //
    // // await pdf.save();
    //
    //
    // // final result = await ref.putFile(File(path));
    // Directory appDocDir = await getApplicationDocumentsDirectory();
    // var filePath = "${appDocDir.path}/testar.pdf";
    // final file = File(filePath);
    // await file.writeAsBytes(await pdf.save());
    //
    // Share.shareFiles(['${appDocDir.path}/testar.pdf'], text: 'Report');
    //
    // print(filePath);
  }

}


class ProjectData extends StatefulWidget {
  const ProjectData({Key? key, this.projectid}) : super(key: key);
  final String? projectid;

  @override
  State<ProjectData> createState() => _ProjectData();
}

class _ProjectData extends State<ProjectData> {
// final String projectid;
// const ProjectData({super.key, required this.projectid});

  List<String> singleProjectData = [];

  @override
  Widget build(BuildContext context) {
      // return Center(
      //   child: Text(projectid),
      // );
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: Text("Projects Data"),
          actions: <Widget>[
          ],
        ),
        body: StreamBuilder(
          // stream: FirebaseFirestore.instance.collection('projects').doc(widget.projectid).collection('project-data').snapshots(),
          stream: FirebaseFirestore.instance.collection('users').doc(FirebaseAuth.instance.currentUser?.uid).collection('projects').doc(widget.projectid).collection('project-data').snapshots(),
          // stream: FirebaseFirestore.instance.collection('users').doc('3KvAlMTz46bhthnKVeedfKsjBRc2').collection('projects').doc('Project_01').collection('project-data').snapshots(),
          builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }

            return ListView(
              children: snapshot.data!.docs.map((document) {
                return ListTile(
                  leading: CircleAvatar(backgroundImage: NetworkImage(document['location-image']),),
                  title: Text(document['location-name']),
                  subtitle: Text(document['location-description']),
                  onTap: () {

                    // String projectid = document['id'];
                    // Navigator.of(context).push(MaterialPageRoute(
                    //   builder: (context) => ProjectData(projectid: projectid),
                    // ));
                  },
                  onLongPress: () {
                    String documentName = document['location-name'];
                    String documentId = document.id;

                    Future<void> _showMyDialog() async {
                      return showDialog<void>(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: const Text('Delete Confirmation'),
                            content: SingleChildScrollView(
                              child: ListBody(
                                children: const <Widget>[
                                  Text('Do you really want to delete this project data?'),
                                ],
                              ),
                            ),
                            actions: <Widget>[
                              TextButton(
                                child: const Text('Yes'),
                                onPressed: () {
                                  // final siteUser = FirebaseFirestore.instance
                                  //             .collection('projects')
                                  //             .doc(widget.projectid)
                                  //             .collection('project-data')
                                  //             .doc(documentId);

                                  final databaseReference = FirebaseFirestore.instance;
                                  final siteUser = databaseReference.collection('users')
                                      .doc(FirebaseAuth.instance.currentUser?.uid)
                                      .collection('projects')
                                      .doc(widget.projectid)
                                      .collection('project-data')
                                      .doc(documentId);

                                          siteUser.delete();

                                          final deleteSucess = SnackBar(
                                            content: const Text('Project Data Deleted'),
                                          );
                                          ScaffoldMessenger.of(context).showSnackBar(deleteSucess);
                                        Navigator.of(context, rootNavigator: true).pop('dialog');
                                },
                              ),
                            ],
                          );
                        },
                      );
                    }

                    _showMyDialog();
                  },
                );
              }).toList(),
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => DataForm(projectid: widget.projectid),
            ));
            // Navigator.pushNamed(context, '/add-data');
          },
          backgroundColor: Colors.blue,
          child: const Icon(Icons.add_sharp),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: Color(0xFF6200EE),
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.white.withOpacity(.60),
          selectedFontSize: 14,
          unselectedFontSize: 14,
          onTap: (value) {
            Navigator.pushNamed(context, '/settings');
          },
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite),
              label: 'School'
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.music_note),
              label: 'School'
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.location_on),
              label: 'School'
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.library_books),
              label: 'School'
            ),
          ],
        )
      );

  }


}




