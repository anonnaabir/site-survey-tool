import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sitesurveytool/login.dart';
import 'package:get/get.dart';

class AppSettings extends StatefulWidget {
  const AppSettings({ Key? key }) : super(key: key);

  @override
  State<AppSettings> createState() => _AppSettings();
}

class _AppSettings extends State<AppSettings> {

  String? userEmail = FirebaseAuth.instance.currentUser?.email;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: Column(
        children: <Widget>[
          Text('Email: '+ userEmail!),
          ElevatedButton(
            onPressed: () {
              Future<void> _ConfirmLogout() async {
                await FirebaseAuth.instance.signOut();
              }

              _ConfirmLogout();

                final logoutMsg = SnackBar(
                  content: const Text('Logout Sucessful.'),
                );
                ScaffoldMessenger.of(context).showSnackBar(logoutMsg);
                Navigator.pushNamed(context, '/splash');
            },
            child: const Text('Logout'),
          ),
        ],
      ),
    );
  }

  Future LogOut() async {
      return AlertDialog(
        title: const Text('Delete Confirmation'),
        content: SingleChildScrollView(
          child: ListBody(
            children: const <Widget>[
              Text('Do you really want to delete this project data?'),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: const Text('Yes'),
            onPressed: () {
              Future ConfirmLogout() async {
                await FirebaseAuth.instance.signOut();
                Get.to(LoginForm());
              }

              ConfirmLogout();
            },
          ),
        ],
      );
  }
}