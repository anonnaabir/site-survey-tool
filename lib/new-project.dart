import 'dart:io';
import 'package:intl/intl.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:path/path.dart' as p;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sitesurveytool/projects.dart';

void main() => runApp(const ProjectForm());

// class NewProject extends StatelessWidget {
//   const NewProject({super.key});
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'New Project',
//       home: ProjectForm(),
//     );
//   }
// }



class ProjectForm extends StatefulWidget {
  const ProjectForm({ Key? key }) : super(key: key);

  @override
  State<ProjectForm> createState() => _ProjectForm();
}


class _ProjectForm extends State<ProjectForm> {
  File? image;

  TextEditingController nameController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  Future pickImage() async {
    final image = await ImagePicker().pickImage(source: ImageSource.gallery);
    if (image == null) return;
    final filePath = File(image.path);
    // print(filePath);
    setState(() => this.image = filePath);
    await _uploadFile(image.path);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Column(
        // children: [
        //   ProjectName(),
        //   ProjectDescription(),
        //   SubmitButton()
        // ],
        children: [
          TextField(
          controller: nameController,
          decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: 'Enter a search term',
            ),
          ),

          TextField(
            controller: descriptionController,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Enter project description',
            ),
          ),

          ElevatedButton(
            // style: style,
            onPressed: () => pickImage(),
            child: const Text('Enabled'),
          ),

            TextButton(
            style: TextButton.styleFrom(
            primary: Colors.blue,
            ),
            onPressed: () {
            // Navigator.pushNamed(context, '/project-data');
            final name = nameController.text;
            final location = descriptionController.text;
            // final imagePath = FileImage(image!);
            createProject(
                name: name,
                location: location,
            );

            final addSucess = SnackBar(
              content: const Text('Project Addedd Successfully'),
            );
            ScaffoldMessenger.of(context).showSnackBar(addSucess);

            Navigator.pop(context, MaterialPageRoute(builder: (context) => ProjectData()));
            // print('Alhamdulillah. Data Sent');
            },

              child: Text('Add Project'),
            )
        ],
      ),
    );
  }

  Future _uploadFile(String path) async {
    final ref = FirebaseStorage.instance.ref()
        .child('projects-image')
        .child('${DateTime.now().toIso8601String() + p.basename(path)}');

    final result = await ref.putFile(File(path));
    final fileUrl = await result.ref.getDownloadURL();
    print(fileUrl);
  }

  Future createProject({required String name, required String location}) async {

    String project_id = name.replaceAll(RegExp(' '), '_');
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy-MM-dd_kk:mm').format(now);

    final FirebaseAuth auth = FirebaseAuth.instance;
    final User user = auth.currentUser!;
    final userID = user.uid;

    final db = FirebaseFirestore.instance;
    final createProject = db.collection("users")
        .doc(userID)
        .collection('projects')
        .doc(project_id);


    final json = {
      // 'id': project_id.toLowerCase() + '_' + formattedDate,
      'id': project_id,
      'name': name,
      'location': location,
    };

    await createProject.set(json);

  }

}