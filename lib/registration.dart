import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

void main() => runApp(const RegisterForm());

class RegisterForm extends StatefulWidget {
  const RegisterForm({ Key? key }) : super(key: key);

  @override
  State<RegisterForm> createState() => _RegisterForm();
}

class _RegisterForm extends State<RegisterForm> {

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Registration'),
      ),
      // body is the majority of the screen.
      body: Column(
        children: [
          TextField(
            controller: emailController,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Enter Your Email',
            ),
          ),

          TextField(
            controller: passwordController,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Enter Your Password',
            ),
            obscureText: true
          ),
          Center(
            child: Column(

              children: <Widget>[
                ElevatedButton(
                  // style: style,
                  onPressed: () => SignUp(),
                  child: const Text('Register'),
                ),
                TextButton(
                  style: TextButton.styleFrom(
                    textStyle: const TextStyle(fontSize: 16),
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/login');
                  },
                  child: const Text("Login"),
                ),
              ],
            ),
          ),
        ],
      )
    );
  }

   Future SignUp() async {
     bool checkInternet = await InternetConnectionChecker().hasConnection;
     if(checkInternet == false) {
       final noInternetMsg = SnackBar(
         content: const Text('No Internet. Please turn on Wifi or Mobile Data.'),
       );
       ScaffoldMessenger.of(context).showSnackBar(noInternetMsg);
     }

    await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: emailController.text.trim(),
        password: passwordController.text.trim()
    );

    final addSucess = SnackBar(
      content: const Text('Registration Sucessful'),
    );
    ScaffoldMessenger.of(context).showSnackBar(addSucess);

    final FirebaseAuth auth = FirebaseAuth.instance;
    final User user = auth.currentUser!;
    final userID = user.uid;
    final userEmail = user.email;

    final db = FirebaseFirestore.instance;
    final createProject = db.collection("users").doc(userID);

    final json = {
      'userid': userID,
      'email': userEmail,
    };

    await createProject.set(json);

     Navigator.pushNamed(context, '/settings');

  }
}