import 'dart:io';
import 'package:intl/intl.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:path/path.dart' as p;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sitesurveytool/projects.dart';

void main() => runApp(const DataForm());

// class AddData extends StatelessWidget {
//   const AddData({super.key});
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Add Data',
//       home: DataForm(),
//     );
//   }
// }



class DataForm extends StatefulWidget {
  // final String projectid;
  // const ProjectData({super.key, required this.projectid});
  // const DataForm({ Key? key}) : super(key: key);
  const DataForm({Key? key, this.projectid}) : super(key: key);
  final String? projectid;

  @override
  State<DataForm> createState() => _DataForm();
}


class _DataForm extends State<DataForm> {
  File? image;

  TextEditingController lnameController = TextEditingController();
  TextEditingController ldescriptionController = TextEditingController();

  Future pickImage() async {
    final image = await ImagePicker().pickImage(source: ImageSource.gallery);
    if (image == null) return;
    final filePath = File(image.path);
    // print(filePath);
    setState(() => this.image = filePath);
    // await _uploadFile(image.path);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Add Data'),
        ),
        body: Column(
          // children: [
          //   ProjectName(),
          //   ProjectDescription(),
          //   SubmitButton()
          // ],
          children: [
            TextField(
              controller: lnameController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Enter Project Name',
              ),
            ),

            TextField(
              controller: ldescriptionController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Enter project description',
              ),
            ),

            ElevatedButton(
              // style: style,
              onPressed: () => pickImage(),
              child: const Text('Enabled'),
            ),

            TextButton(
              style: TextButton.styleFrom(
                primary: Colors.blue,
              ),
              onPressed: () {
                // Navigator.pushNamed(context, '/project-data');
                final name = lnameController.text;
                final description = ldescriptionController.text;
                // final imagePath = FileImage(image!);
                addProjectData(
                  lname: name,
                  ldescription: description,
                );

                final addSucess = SnackBar(
                  content: const Text('Data Addedd Successfully'),
                );
                ScaffoldMessenger.of(context).showSnackBar(addSucess);

                // Navigator.pushNamed(context, '/project-data');
                Navigator.pop(context, MaterialPageRoute(builder: (context) => ProjectData()));
                // print('Alhamdulillah. Data Sent');
              },

              child: Text('Submit Form'),
            )
          ],
        )
    );
  }

  Future _uploadFile(String path) async {
    final ref = FirebaseStorage.instance.ref()
        .child('projects-image')
        .child('${DateTime.now().toIso8601String() + p.basename(path)}');

    final result = await ref.putFile(File(path));
    final fileUrl = await result.ref.getDownloadURL();
    print(fileUrl);
  }

  Future addProjectData({required String lname, required String ldescription}) async {

    final ref = FirebaseStorage.instance.ref()
        .child('projects-image')
        .child('${DateTime.now().toIso8601String() + p.basename(image!.path)}');

    final result = await ref.putFile(File(image!.path));
    final fileUrl = await result.ref.getDownloadURL();

    final projectID = widget.projectid;

    String data = lname.replaceAll(RegExp(' '), '_');


    final dataList = {
      'id': lname,
      'location-name': lname,
      'location-description': ldescription,
      'location-image': fileUrl,
      "timestamp": FieldValue.serverTimestamp(),
    };

    final databaseReference = FirebaseFirestore.instance;
    // final newData = databaseReference.collection('projects')
    //     .doc(projectID)
    //     .collection('project-data').doc();

    final newData = databaseReference.collection('users')
        .doc(FirebaseAuth.instance.currentUser?.uid)
        .collection('projects')
        .doc(widget.projectid)
        .collection('project-data')
        .doc();

    await newData.set(dataList);
    // await createData.set(dataList);
  }

}