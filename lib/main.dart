import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:sitesurveytool/add-data.dart';
import 'package:sitesurveytool/home.dart';
import 'package:sitesurveytool/login.dart';
import 'package:sitesurveytool/projects.dart';
// import 'package:sitesurveytool/project-data.dart';
import 'package:sitesurveytool/new-project.dart';
import 'package:sitesurveytool/registration.dart';
import 'package:sitesurveytool/settings.dart';
import 'package:sitesurveytool/splash-screen.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(const SiteSurveyTool());
}

// void main() => runApp(const SiteSurveyTool());

class SiteSurveyTool extends StatelessWidget {
  const SiteSurveyTool({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.green,
        useMaterial3: true
      ),
      title: 'Site Survey Tool',
      home: HomeContent(),
      routes: {
        '/splash': (context) => const SplashScreen(),
        '/home': (context) => const Home(),
        '/login': (context) => const LoginForm(),
        '/registration': (context) => const RegisterForm(),
        '/projects': (context) => const Projects(),
        // '/project-data': (context) => const ProjectData(),
        '/new-project': (context) => const ProjectForm(),
        '/add-data': (context) => const DataForm(),
        '/settings': (context) => const AppSettings(),
      },
    );
  }
}


class HomeContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<User?>(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (context, snapshot){
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          }
          if (snapshot.connectionState == ConnectionState.none) {
            return Center(child: Text('No Internet. Please turn on your Wifi or Mobile Data.'));
          }
          if (snapshot.hasError){
            return Center(child: Text('Something went wrong. Please try again'));
          }
          if (snapshot.hasData){
            return Projects();
          }
          else {
            return SplashScreen();
          }
        },
      ),
    );
  }
}