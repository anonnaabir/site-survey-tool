import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

void main() => runApp(const LoginForm());

class LoginForm extends StatefulWidget {
  const LoginForm({ Key? key }) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginForm();
}

class _LoginForm extends State<LoginForm> {

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey.shade800,
        // body is the majority of the screen.
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Text(
                'Email',
                textAlign: TextAlign.left,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16.0),
              child: TextField(
                controller: emailController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Enter Your Email',
                  hintStyle: TextStyle(
                   color: Colors.white
                  )
                ),
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),

            Padding(
              padding: EdgeInsets.all(16.0),
              child: Text(
                'Password',
                textAlign: TextAlign.left,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),

            Padding(
              padding: EdgeInsets.all(16.0),
              child: TextField(
                controller: passwordController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Enter Your Password',
                    hintStyle: TextStyle(
                    color: Colors.white
                    )
                ),
                  obscureText: true
              ),
            ),

            Center(
              child: Column(

                children: <Widget>[
                  ElevatedButton(
                    // style: style,
                    onPressed: () => SignIn(),
                    child: const Text('Login'),
                  ),
                  TextButton(
                    style: TextButton.styleFrom(
                      textStyle: const TextStyle(fontSize: 16),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/home');
                    },
                    child: const Text("Register Now"),
                  ),
                ],
              ),
            ),

          ],
        )
    );
  }

  Future SignIn() async {
    bool checkInternet = await InternetConnectionChecker().hasConnection;
    if(checkInternet == false) {
      final noInternetMsg = SnackBar(
        content: const Text('No Internet. Please turn on Wifi or Mobile Data.'),
      );
      ScaffoldMessenger.of(context).showSnackBar(noInternetMsg);
    }

    await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: emailController.text.trim(),
        password: passwordController.text.trim()
    );

    Navigator.pushNamed(context, '/home');
    
    print('Sign in done.Alhamdulillah');
  }
}